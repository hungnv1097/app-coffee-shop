import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import { BASE_URL } from "../services/host";

export default function BlogItem(props) {
  return (
    // <View style={styles.container}>
      <TouchableOpacity onPress={props.onSelect}>
        <View style={styles.container}>
        <View style={styles.imageContainer}>
          <ImageBackground
            source={{ uri: BASE_URL + props.item.imageURL }}
            resizeMode="cover"
            style={styles.image}
            imageStyle={{
               borderTopLeftRadius: 15,
              borderTopRightRadius: 15, 
            }}
          />
        </View>
        <View style={styles.detail}>
          <Text style={styles.title}>{props.item.title}</Text>
          <Text style={styles.content}>
            {props.item.content}
          </Text>
        </View>
        </View>
      </TouchableOpacity>
    // </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: 200,
    height: 260,
    borderRadius: 15,
    margin: 5,
    // alignItems: 'center',
    justifyContent: "center",
    // padding: 10,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5
  },
  image: {
    width: "100%",
    height: "100%",
  },
  imageContainer: {
    flex: 5,
  },
  detail: {
    flex: 5,
    padding: 10
    // height: 60,
    // flexDirection: "column"
    // justifyContent: "space-between",
    // alignItems: "flex-end"
  },
  title: {
    fontSize: 15,
    flex: 4,
    textTransform: 'uppercase'
  },
  content: {
    color: "blue",
    fontSize: 15,
    flex: 6
  },
  description: {
    color: "gray",
    fontSize: 15
  }
});
