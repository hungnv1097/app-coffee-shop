import React, { useState } from "react";
import { View } from 'react-native';
import Dialog from "react-native-dialog";

export default function DialogOrder(props) {
  const [phone, setPhone] = useState('');
  const [address, setaddress] = useState('');
  return (
    <View>
      <Dialog.Container visible= {props.dialogVisible}>
        <Dialog.Title>Thông tin nhận hàng</Dialog.Title>
          <Dialog.Input 
            label="Số điện thoại"
            value= {phone}
            onChangeText={phone => setPhone(phone)}
            keyboardType={'numeric'}
          />
          <Dialog.Input 
            label="Địa chỉ" 
            value= {address}
            onChangeText={addr => setaddress(addr)}
          />
        <Dialog.Button
          onPress= { props.closeDialog }
          label="Cancel" 
        />
        <Dialog.Button
          onPress = {() => props.submitOrder(phone, address)}
          label="Submit" 
        />
      </Dialog.Container>
    </View>
  );
}
