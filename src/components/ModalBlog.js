import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  Image,
  Dimensions
} from "react-native";
import Modal from "react-native-modalbox";
import { BASE_URL } from "../services/host";

export default class ModalBlog extends Component {
  constructor(props) {
    super(props);
  }
  openModal = () => {
    this.refs.modal.open();
  };
  closeModal = () => {
    this.refs.modal.close();
  };
  render() {
    const { blog } = this.props;
    return (
      <Modal style={styles.container} position={"center"} ref={"modal"}>
        <Text style={styles.title}>{blog.title}</Text>
        <Image
          style={styles.img}
          source={{ uri: BASE_URL + blog.imageURL }}
        />
        <Text style={styles.content}>{blog.content}</Text>
      </Modal>
    );
  }
}
const screen = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    height: screen.height - 80,
    width: screen.width - 40,
    borderRadius: 10,
    padding: 15
  },
  title: {
    fontSize: 18,
    color: "rgb(242, 197, 114)",
    textAlign: "center",
    textTransform: "uppercase"
  },
  img: {
    width: "100%",
    height: 200
  },
  content: {
    textAlign: "center"
  }
});
