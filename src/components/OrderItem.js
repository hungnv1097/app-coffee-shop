import React from "react";
import { 
  View, 
  Text, 
  StyleSheet, 
  TouchableOpacity 
} from "react-native";

export default function OrderItem(props) {
  const { products, _id, order_date } = props.item;
  const code = _id.slice(18, 30);
  const date = new Date(order_date);
  const convertDate = date.toISOString().slice(0, 10);

  const listProduct = products.map((prd, index) => (
    <Text key={index}>{prd.name}</Text>
  ));

  return (
    <TouchableOpacity onPress={props.removeItem}>
      <Text style={styles.code}>#{code}</Text>
      <View style={styles.container}>
        <View style={styles.productOrder}>{listProduct}</View>
        <View style={styles.inforOrder}>
          <Text>Ngày đặt: {convertDate}</Text>
          <Text style={styles.price}>Tổng tiền: {props.item.total}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    padding: 5,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "gray"
  },
  name: {
    fontWeight: "bold",
    fontSize: 16,
    textTransform: "uppercase"
  },
  inforOrder: {
    flex: 5,
    alignContent: "center",
    justifyContent: "center",
    paddingLeft: 10
  },
  code: {
    color: "rgb(242, 197, 114)",
    fontWeight: "bold",
    fontSize: 16,
    padding: 2
  },
  description: {
    color: "gray",
    fontSize: 15
  },
  productOrder: {
    flex: 5
  },
  price: {
    fontWeight: "bold"
  }
});
