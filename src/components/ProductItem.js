import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { BASE_URL } from '../services/host';

export default function ProductItem(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={props.onSelect}
      >
        <View>
          <View style={styles.imageContainer}>
            <Image
              source={{uri: BASE_URL + props.item.image}}
              style={styles.image}
            />
          </View>
          <View style={styles.detail}>
            <Text style={styles.name}>{props.item.name}</Text>
            <Text style={styles.price}>{props.item.price}$</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: 120,
    height: 160,
    borderRadius: 15,
    margin: 5,
    // alignItems: 'center',
    justifyContent: "center",
    padding: 10,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5
  },
  imageContainer: {
    width: 100,
    height: 70, 
  },
  image: {
    width: "100%",
    height: "100%"
  },
  name: {
    fontWeight: "bold",
    fontSize: 15,
    flex: 8
  },
  detail: {
    paddingTop: 5,
    width: "100%",
    // height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end"
  },
  price: {
    color: "blue",
    fontSize: 15,
    flex: 2
  },
  description: {
    color: "gray",
    fontSize: 15
  }
});
