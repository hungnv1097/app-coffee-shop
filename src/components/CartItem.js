import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { BASE_URL } from '../services/host';
import { Ionicons } from '@expo/vector-icons';

export default function CartItem(props) {
  const { size } = props.item;

  const listSize = size.map((item, index) =>
    <Text key={index}>{item.value}</Text>
  );

  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          source={{uri: BASE_URL + props.item.image}}
          style={styles.image}
        />
      </View>
      <View style={styles.detail}>
        <Text style={styles.name}>{props.item.name}</Text>
        <Text style={styles.price}>{props.item.price}$</Text>
      </View>
      <View style={styles.size}>
        {listSize}
      </View>
      <View style={styles.amount}>
        <Text style={styles.textAmount}>{props.item.totalAmount}</Text>
      </View>
      <View style={styles.removeItem}>
        <TouchableOpacity onPress={props.removeItem}>
          <Ionicons name="md-trash" size={25} color="#000" />
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    // alignItems: 'center',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  imageContainer: {
    flex: 3,
    width: 100,
    height: 70, 
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 5
  },
  name: {
    fontWeight: "bold",
    fontSize: 16,
    textTransform: 'uppercase'
  },
  detail: {
    flex: 6,
    alignContent: 'center',
    justifyContent: 'center',
    paddingLeft: 10
  },
  price: {
    color: 'rgb(242, 197, 114)',
    fontSize: 16,
  },
  description: {
    color: "gray",
    fontSize: 15
  },
  size: {
    flex: 2,
    justifyContent: 'center'
  },
  amount: {
    flex: 2,
    justifyContent: 'center',
  },
  textAmount: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'gray',
    textAlign: 'center',
    padding: 5,
    width: 30,
    height: 30
  },
  removeItem: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
