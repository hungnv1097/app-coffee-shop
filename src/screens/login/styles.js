import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
      paddingTop: 20,
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    loginGG: {
      padding: 15,
      paddingLeft: 60,
      paddingRight: 60,
      backgroundColor: "#fff",
      borderRadius: 20,
      borderWidth: 2,
      borderColor: "#fff",
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5
    },
    text: {
      paddingTop: 20,
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      color: "#fff",
      fontWeight: "bold"
    },
    late: {
      paddingTop: 20,
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      color: "rgb(242, 197, 114)",
      fontWeight: "bold"
    }
});