import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import * as Google from "expo-google-app-auth";
import { connect } from "react-redux";
import { login } from "../../actions/auth";
import styles from './styles';

class LoginScreen extends Component {
  signInWithGoogleAsync = async () => {
    try {
      const result = await Google.logInAsync({
        // androidClientId: '',
        iosClientId:
          "978721442416-njv2bfahc39lug8mo7sb4e5ss4d3d8m6.apps.googleusercontent.com",
        scopes: ["profile", "email"]
      });

      if (result.type === "success") {
        const { login } = this.props;
        login(result.user);
      } else {
        return;
      }
    } catch (e) {
      return { error: true };
    }
  };

  render() {
    const { isLogin, navigation } = this.props;

    if (isLogin) {
      return navigation.navigate("Home");
    }
    return (
      <ImageBackground
        source={require("../../assets/images/bg1.jpg")}
        resizeMode="cover"
        blurRadius={1}
        style={{ width: "100%", height: "100%" }}
      >
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.loginGG}
            onPress={() => this.signInWithGoogleAsync()}
          >
            <Text style={{ color: "#000", fontWeight: "bold" }}>
              Sign In With Google
            </Text>
          </TouchableOpacity>
          <Text style={styles.text}> Hoặc </Text>
          <TouchableOpacity
            style={styles.goHome}
            onPress={() => navigation.navigate("Home")}
          >
            <Text style={styles.late}>Đăng nhập sau</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

LoginScreen.navigationOptions = {
  header: null
};

const mapStateToProps = state => {
  return {
    isLogin: state.auth.isLogin,
    user: state.auth.user
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    login: user => dispatch(login(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
