import React, { Component } from "react";
import {
  Text,
  Image,
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";

import OrderItem from "../../components/OrderItem";
import { getOrder } from '../../actions/order';
import styles from './styles';

class UserScreen extends Component {
  state = {
    isRefresh: false
  }
  componentDidMount() {
    const { getListOrder, user } = this.props;
    getListOrder(user.email);
  }
  handleLogOut = () => {
    const { logout, navigation } = this.props;
    logout();
    navigation.navigate("Login");
  };
  onRefresh = async () => {
    const { getListOrder, user } = this.props;
    this.setState({
      isRefresh: true
    });
    await getListOrder(user.email);
    this.setState({
      isRefresh: false
    });
  }
  render() {
    const { user, isLogin, listOrder, navigation, isLoading } = this.props;
    const { isRefresh } = this.state;

    if(!isLogin) {
      return (
        <View style={styles.messageBox}>
          <Text style={styles.title}>Bạn chưa đăng nhập. Vui lòng</Text>
          <TouchableOpacity
            styleDisabled={{ color: "red" }}
            onPress={() => navigation.navigate('Login')}
            style={styles.goLogin}
          >
            <Text style={styles.loginText}>Đăng nhập</Text>
          </TouchableOpacity>
        </View>
      )
    }
    if (isLoading) {
      return (
        <View style={styles.centered}>
          <ActivityIndicator size="large"/>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.logout}>
          <TouchableOpacity
            style={styles.btnLogout}
            onPress={() => this.handleLogOut()}
          >
            <Text>Logout</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.headerProfile}>
          <Image
            source={{ uri: user.photoUrl }}
            style={styles.image}
          />
          <Text style={styles.nameUser}>{user.name}</Text>
        </View>
        <View style={styles.information}>
          <Text style={styles.headerInfor}>Thông tin cá nhân</Text>
          <View style={styles.bodyInfor}>
            <View style={styles.detail}>
              <Text>Tên: {user.name}</Text>
            </View>
            <View style={styles.detail}>
              <Text>Sinh nhật</Text>
            </View>
            <View style={styles.detail}>
              <Text>Email: {user.email}</Text>
            </View>
          </View>
        </View>
        <View style={styles.order}>
          <Text style={styles.headerInfor}>Thông tin đặt hàng</Text>
          <FlatList
            onRefresh={this.onRefresh}
            refreshing={isRefresh}
            data={listOrder}
            keyExtractor={item => item._id}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <OrderItem item={item} removeItem={() => this.removeItem} />
            )}
          />
        </View>
      </View>
    );
  }
}

UserScreen.navigationOptions = {
  header: null
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    listOrder: state.cart.order_user,
    isLogin: state.auth.isLogin,
    isLoading: state.cart.isLoading
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    logout: () =>
      dispatch({
        type: "LOGOUT"
    }),
    getListOrder: email => dispatch(getOrder(email))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserScreen);
