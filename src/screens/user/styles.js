import { StyleSheet } from "react-native";

export default StyleSheet.create({
    centered: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    container: {
      flex: 1,
      paddingTop: 20,
      fontSize: 20
    },
    headerProfile: {
      flex: 2,
      alignItems: "center",
      padding: 30,
      paddingTop: 0
    },
    image: {
      borderRadius: 50,
      height: 80,
      width: 80
    },
    information: {
      flex: 3
    },
    headerInfor: {
      backgroundColor: "rgb(242, 197, 114)",
      padding: 5,
      color: "#fff",
      fontWeight: "bold",
      fontSize: 20
    },
    bodyInfor: {
      paddingLeft: 5,
      paddingRight: 5
    },
    detail: {
      borderBottomColor: "#D8D8D8",
      borderBottomWidth: 1,
      width: "100%",
      padding: 10
    },
    order: {
      flex: 5
    },
    logout: {
      flex: 1,
      alignItems: "flex-end",
      padding: 8
    },
    btnLogout: {
      borderWidth: 1,
      borderRadius: 5,
      padding: 5
    },
    messageBox: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    goLogin: {
      marginTop: 10,
      padding: 10,
      paddingLeft: 50,
      paddingRight: 50,
      backgroundColor: "#1E6738",
      borderRadius: 20,
      borderWidth: 1,
      borderColor: "#fff",
      color: "#ffffff"
    },
    title: {
      fontSize: 18,
    }, 
    loginText: {
      color: "#fff",
      textAlign: "center",
      fontSize: 16,
      paddingLeft: 10,
      paddingRight: 10
    }, 
  });