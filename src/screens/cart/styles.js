import { StyleSheet } from "react-native";

export default StyleSheet.create({
  letOrder: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  goHome: {
    marginTop: 10,
    padding: 10,
    paddingLeft: 50,
    paddingRight: 50,
    backgroundColor: "#1E6738",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#fff",
    color: "#ffffff"
  },
  title: {
    fontSize: 18
  },
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  total: {
    textTransform: "uppercase",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 5,
    paddingBottom: 5
  },
  textTotal: {
    fontSize: 18,
    fontWeight: "300"
  },
  totalCost: {
    fontSize: 22,
    fontWeight: "bold",
    color: "rgb(242, 197, 114)"
  },
  OrderText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10
  },
  order: {
    alignContent: "flex-end",
    flex: 2,
    width: "100%",
    padding: 8,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    backgroundColor: "#fff"
  },
  orderButton: {
    backgroundColor: "#1E6738",
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#fff",
    padding: 20,
    color: "#ffffff"
  }
});
