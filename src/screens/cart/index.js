import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import CartItem from "../../components/CartItem";
import DialogOrder from "../../components/DialogOrder";
import { removeItem, getTotal } from '../../actions/cart';
import { submit } from '../../actions/order';
import styles from './styles';

class CartScreen extends Component {
  state = {
    dialogVisible: false
  };
  componentWillMount() {
    this.props.getTotal();
  }
  removeItem = (key, cost) => {
    this.props.removeItem(key, cost);
  };
  handleDialog = () => this.setState({ dialogVisible: !this.state.dialogVisible });

  handleSubmit = (phone, address) => {
    const { listItem, total, user, submitOrder, error, navigation} = this.props;
    const data = {
      products: [...listItem],
      total,
      phone,
      address,
      email: user.email
    }
    submitOrder(data);
    if(!error) {
      this.handleDialog();
      alert('Đặt hàng thành công!');
      navigation.navigate('Home');
    }
  }
  render() {
    const { listItem, total, navigation } = this.props;
    const { dialogVisible } = this.state;
  
    if(total == 0) {
      return (
        <View style={styles.letOrder}>
          <Text style={styles.title}>Bạn chưa chọn món gì.</Text>
          <Text style={styles.title}>Hãy chọn món ngay !</Text>
          <TouchableOpacity
            styleDisabled={{ color: "red" }}
            onPress={() => navigation.navigate('Home')}
            style={styles.goHome}
          >
            <Text style={styles.OrderText}>Menu</Text>
          </TouchableOpacity>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View style={{ flex: 8, padding: 10 }}>
          <FlatList
            data={listItem}
            keyExtractor={item => item._id}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <CartItem
                item={item}
                removeItem={() => this.removeItem(item._id, item.totalCost)}
              />
            )}
          />
        </View>
        <View style={styles.order}>
          <View style={styles.total}>
            <Text style={styles.textTotal}>Tổng cộng</Text>
            <Text style={styles.totalCost}>${total}</Text>
          </View>
          <TouchableOpacity
            styleDisabled={{ color: "red" }}
            onPress={() => this.handleDialog()}
            style={styles.orderButton}
          >
            <Text style={styles.OrderText}>Đặt hàng</Text>
          </TouchableOpacity>
        </View>
        <DialogOrder 
          dialogVisible = { dialogVisible}
          closeDialog = {this.handleDialog}
          submitOrder = { this.handleSubmit }
        />
      </View>
    );
  }
}

CartScreen.navigationOptions = {
  title: "Giỏ hàng"
};

const mapStateToProps = state => {
  return {
    listItem: state.cart.listItem,
    total: state.cart.total,
    user: state.auth.user,
    error: state.cart.error
  };
};
const mapDispatchToProps = dispatch => {
  return {
    removeItem: (id, cost) => dispatch(removeItem(id, cost)),
    getTotal: () => dispatch(getTotal()),
    submitOrder: data => dispatch(submit(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);
