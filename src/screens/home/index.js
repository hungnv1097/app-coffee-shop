import React, { Component } from "react";
import {
  FlatList,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  ImageBackground,
} from "react-native";

import { connect } from "react-redux";
import ProductItem from "../../components/ProductItem";
import BlogItem from '../../components/BlogItem';
import ModalBlog from '../../components/ModalBlog';
import { fetchData, add_current } from '../../actions/product';
import styles from './styles';

class HomeScreen extends Component {
  state = {
    products: [],
    modalVisible: false,
    blog: {}
  };
  componentDidMount() {
    this.fetch();
  }

  fetch = () => {
    this.props.getData();
  };
  onSelectItem = key => {
    const { products } = this.props;
    const { navigate } = this.props.navigation;

    const product = products.filter(prd => prd._id === key);
    const data = product[0];
    this.props.addCurrent(data);
    navigate("Product");
  };
  onOpenModal = id => {
    const { blogs } = this.props;
    const result = blogs.filter(i => i._id === id);
    this.setState({ blog: result[0] })
    this.refs.myModal.openModal();
  }
  closeModal = () => this.setState({ modalVisible: false });

  render() {
    const { products, blogs, isLoading } = this.props;
    const { blog } = this.state;
    
    if (isLoading) {
      return (
        <View style={styles.centered}>
          <ActivityIndicator size="large"/>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <ScrollView style={{height: '100%'}}>
          <View style={styles.imageBackground}>
            <ImageBackground
              source={require("../../assets/images/bg_2.jpg")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            >
              <View style={styles.wellcome}>
                <Text style={styles.textWellcome}>
                  A good day starts with a good coffee.
                </Text>
              </View>
            </ImageBackground>
          </View>
          <View style={styles.body}>
            <FlatList
              horizontal
              data={products}
              keyExtractor={item => item._id}
              showsHorizontalScrollIndicator={false}
              style={styles.sliderProduct}
              renderItem={({ item }) => (
                <ProductItem
                  item={item}
                  onSelect={() => this.onSelectItem(item._id)}
                />
              )}
            />
            <View style={styles.blogContainer}>
              <View style={styles.blogHeader}>
                <Text style={styles.textHeader}>Tin tức</Text>
              </View>
              <View style={styles.blogCard}>
                <ImageBackground
                  source={require("../../assets/images/blog1.jpg")}
                  resizeMode="cover"
                  style={{ width: "100%", height: 200 }}
                  imageStyle={{ borderRadius: 5 }}
                >
                  <View style={styles.viewCard}>
                    <Text style={styles.textCard}>
                      A good day starts with a good coffee.
                    </Text>
                    <TouchableOpacity style={styles.readMore}>
                        <Text style={styles.textReadMore}>Read more</Text>
                    </TouchableOpacity>
                  </View>
                </ImageBackground>
              </View>
              <FlatList
                horizontal
                data={blogs}
                keyExtractor={item => item._id}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => (
                <BlogItem
                  item={item}
                  onSelect={() => this.onOpenModal(item._id)}
                />
              )}
            />
            </View>
          </View>
        </ScrollView>
        <ModalBlog 
          ref={'myModal'} 
          parentFlatList={this}
          blog = {blog}
        />
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  header: null
};

const mapStateToProps = state => {
  return {
    products: state.product.products,
    isLoading: state.product.isLoading,
    blogs: state.product.blogs,
    user: state.auth.user
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getData: () => dispatch(fetchData()),
    addCurrent: product => {
      dispatch(add_current(product))
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
