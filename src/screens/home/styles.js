import { StyleSheet } from "react-native";

export default StyleSheet.create({
    centered: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    container: {
      flex: 1,
    },
    imageBackground: {
      flex: 3,
      height: 200,
      justifyContent: "center",
    },
    wellcome: {
      backgroundColor: "transparent",
      textAlign: "center",
      padding: 40
    },
    textWellcome: {
      fontSize: 24,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center"
    },
    viewCard: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    },
    textCard: {
      fontSize: 20,
      fontWeight: "bold",
      color: "#fff",
      textAlign: 'center',
      shadowOpacity: 1
    },
    body: {
      position: "relative",
      paddingTop: 140
    },
    sliderProduct: {
      position: "absolute",
      top: -40,
    },
    blogCard: {
      margin: 5,
      borderRadius: 10,
      padding: 5
    },
    textHeader: {
      color: "rgb(242, 197, 114)",
      fontSize: 17,
      padding: 5,
      paddingLeft: 10,
      fontWeight: "bold"
    },
    readMore: {
      padding: 5,
      borderWidth: 1,
      borderColor: '#fff',
      shadowOpacity: 1
    },
    textReadMore: {
      color: "#fff",
    },
    // blogContainer: {
    //   padding: 10
    // }
    modal3: {
      height: 300,
      width: 300
    },
    btn: {
      margin: 10,
      backgroundColor: "#3B5998",
      color: "white",
      padding: 10
    },
  });