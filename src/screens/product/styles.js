import { StyleSheet } from "react-native";

export default StyleSheet.create({
    productContainer: {
      flex: 9
    },
    centered: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    cartText: {
      color: "#fff",
      textAlign: "center",
      paddingLeft: 10,
      paddingRight: 10
    },
    addCart: {
      alignContent: "flex-end",
      alignItems: "center",
      flex: 1,
      bottom: 0,
      width: "100%",
      padding: 8
    },
    buttonCart: {
      backgroundColor: "#1E6738",
      borderRadius: 15,
      borderWidth: 1,
      borderColor: "#fff",
      padding: 20,
      color: "#ffffff",
      width: "100%"
    },
    namePrd: {
      fontSize: 20,
      fontWeight: "bold"
    },
    title: {
      padding: 15,
      paddingRight: 25,
      justifyContent: "center",
      flex: 6
    },
    after: {
      borderStyle: "solid",
      borderWidth: 1,
      height: 1,
      width: 25,
      marginTop: 10,
      marginBottom: 15
    },
    container: {
      flex: 1,
      backgroundColor: "#FFCC99"
    },
    product: {
      paddingTop: 10,
      paddingBottom: 20,
      flex: 3,
      backgroundColor: "#fff",
      marginBottom: 10,
      borderBottomLeftRadius: 40,
      borderBottomRightRadius: 40,
      flexDirection: "row"
    },
    control: {
      flex: 6,
      backgroundColor: "#fff",
      margin: 10,
      marginLeft: 15,
      marginRight: 15,
      borderRadius: 20,
      paddingBottom: 50
    },
    imageContainer: {
      width: 100,
      height: 120,
      borderRadius: 5
    },
    image: {
      flex: 3,
      justifyContent: "center"
    },
    size: {
      padding: 15,
      paddingTop: 0,
    },
    sizeHeader: {
      fontSize: 15,
      fontWeight: "bold",
      paddingBottom: 15
    },
    sizeDetail: {
      flexDirection: "row",
      justifyContent: "space-around",
      alignItems: "center",
      paddingBottom: 5
    },
    price: {
      padding: 15,
      paddingBottom: 0,
      flexDirection: 'row'
    },  
    priceText: {
      color: 'rgb(242, 197, 114)',
      fontSize: 16,
      fontWeight: 'bold'
    }
});