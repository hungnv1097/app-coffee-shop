import React, { Component } from "react";
import {
  Text,
  TouchableOpacity,
  Image,
  View,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import InputSpinner from "react-native-input-spinner";

import { BASE_URL } from "../../services/host";
import { addToCart } from '../../actions/cart';
import styles from './styles';

class ProductScreen extends Component {
  state = {
    size: [
      {
        value: "S",
        amount: 1
      }
    ],
    totalAmount: 1,
    totalCost: 0
  };
  handleAddCart = () => {
    const { current } = this.props;
    const price = current.price;
    const { navigate } = this.props.navigation;
    const { size } = this.state;

    let totalAmount = 0;
    size.map(item => {
      totalAmount += item.amount;
    });
    current["size"] = size;
    current["totalAmount"] = totalAmount;
    current["totalCost"] = totalAmount * price;

    this.props.addToCart(current);
    navigate("Cart");
  };
  handleChangeSize = (value, amount) => {
    const { size } = this.state;
    const tmpState = size.filter(item => item.value !== value);
    const tmp = { value: value, amount };
    tmpState.push(tmp);

    this.setState({ size: tmpState });
  };
  isEmpty = (obj) => {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  }
  render() {
    const { current } = this.props;

    if(this.isEmpty(current)){
      return (
        <View style={styles.centered}>
          <ActivityIndicator size="large"/>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.productContainer}>
            <View style={styles.product}>
              <View style={styles.title}>
                <Text style={styles.namePrd}>{current.name}</Text>
                <Text style={styles.after}></Text>
                <Text>{current.describe}</Text>
              </View>
              <View style={styles.image}>
                <Image
                  style={styles.imageContainer}
                  source={{ uri: BASE_URL + current.image }}
                />
              </View>
            </View>
            <View style={styles.control}>
              <View style={styles.price}>
                <Text style={styles.sizeHeader}>Price: </Text>
                <Text style={styles.priceText}>{current.price}$</Text>
              </View>
              <View style={styles.size}>
                <Text style={styles.sizeHeader}>Size</Text>
                <View style={styles.sizeDetail}>
                  <Text style={{ fontSize: 15 }}>S</Text>
                  <InputSpinner
                    max={10}
                    min={1}
                    step={1}
                    editable={false}
                    value="1"
                    onChange={num => this.handleChangeSize("S", num)}
                  />
                </View>
                <View style={styles.sizeDetail}>
                  <Text style={{ fontSize: 15 }}>M</Text>
                  <InputSpinner
                    max={10}
                    min={1}
                    step={1}
                    editable={false}
                    value="0"
                    onChange={num => this.handleChangeSize("M", num)}
                  />
                </View>
                <View style={styles.sizeDetail}>
                  <Text style={{ fontSize: 15 }}>L</Text>
                  <InputSpinner
                    max={10}
                    min={1}
                    step={1}
                    editable={false}
                    value="0"
                    onChange={num => this.handleChangeSize("L", num)}
                  />
                </View>
              </View>
            </View>
        </View>
        <View style={styles.addCart}>
          <TouchableOpacity
            styleDisabled={{ color: "red" }}
            onPress={() => this.handleAddCart()}
            style={styles.buttonCart}
          >
            <Text style={styles.cartText}> Add to Cart</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
ProductScreen.navigationOptions = {
  header: null
};

const mapStateToProps = state => {
  return {
    current: state.product.current
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addToCart: data => dispatch(addToCart(data))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductScreen);
