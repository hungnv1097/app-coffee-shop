import { ADD_PRODUCT, ADD_CURRENT } from '../actions/product';

const initialState = {
    products: [],
    blogs: [],
    isLoading: true,
    current: {}
}

export default productReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_PRODUCT:
            return {
                ...state,
                products: action.products,
                blogs: action.blogs,
                isLoading: false
            }
        case ADD_CURRENT:
            return {
                ...state,
                current: action.data
            }
        default:
            return state
   }
}
