import { combineReducers } from 'redux';

import productReducer from './product';
import cartReducer from './cart';
import AuthReducer from './auth';

const rootReducer = combineReducers({
  product: productReducer,
  cart: cartReducer,
  auth: AuthReducer,
});

export default rootReducer;