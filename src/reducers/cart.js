import { ORDER_SUCCESS, ORDER_ERROR, ORDER_USER } from '../actions/order';
import { ADD_TO_CART, REMOVE_ITEM, GET_TOTAL } from '../actions/cart';

const initialState = {
    listItem: [],
    total: 0,
    error: false,
    order_user: [],
    isLoading: true,
}

export default cartReducer = (state = initialState, action) => {
    let tmpState = state.listItem;
    switch(action.type) {
        case ADD_TO_CART:
            let newState = tmpState.filter(i => i._id !== action.data._id);
            return {
                ...state,
                listItem: [...newState, action.data],
                total: state.total + action.data.totalCost,
            }
        case REMOVE_ITEM:
            tmpState = state.listItem;
            newState = tmpState.filter(i => i._id !== action.id);
            return {
                ...state,
                listItem: newState,
                total: state.total - action.cost
            }
        case GET_TOTAL: 
            let tmptotal = 0;
            tmpState.filter(i => {
                tmptotal += i.totalCost
            });
            return {
                ...state,
                total: tmptotal
            }
        case ORDER_SUCCESS: 
            return {
                ...state,
                total: 0,
                listItem: []
            }
        case ORDER_ERROR:
            return {
                ...state,
                error: true
            }
        case ORDER_USER: 
            return {
                ...state,
                order_user: action.data,
                isLoading: false
            }
        default:
            return state
   }
}
