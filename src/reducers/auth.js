import { LOGIN, LOGOUT } from '../actions/auth';


const initialState = {
    user: {},
    isLogin: false
}

export default AuthReducer = (state = initialState, action) => {
    switch(action.type) {
        case LOGIN:
            return {
                user: action.user,
                isLogin: true
            }
        case LOGOUT:
            return {
                user: {},
                isLogin: false
            }
        default:
            return state
   }
}
