import instance from "../../services/base";

export async function getProduct() {
  return await instance
    .get("/product")
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
}

export async function getBlogs() {
  return await instance
    .get("/blog")
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
}