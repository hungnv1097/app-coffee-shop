import { put, takeEvery, call } from 'redux-saga/effects';
import {
    getProduct,
    getBlogs
} from './service';
import { FETCH_PRODUCT, addData } from '../../actions/product';

function* fetchData() { 
    try { 
        const products = yield call(getProduct);
        const blogs = yield call(getBlogs);

        yield put(addData(products, blogs)) 
    } catch (e) { 
        yield put(addData()) 
    }
} 
export function* getProducts() { 
    yield takeEvery(FETCH_PRODUCT, fetchData) 
}
