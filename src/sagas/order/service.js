import instance from "../../services/base";

export async function submitOrder(data) {
  return await instance
    .post("/order/save", data)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
}

export async function getOrder(email) {
  return await instance
    .get("/order?email=" + email)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
}
