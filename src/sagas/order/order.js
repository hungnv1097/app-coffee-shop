import { put, takeEvery, call, takeLatest } from 'redux-saga/effects';
import {
    submitOrder,
    getOrder
} from './service';
import { SUBMIT_ORDER, orderSuccess, orderError, GET_ORDER_USER, listOrderUser } from '../../actions/order';

function* submit(action) { 
    try { 
        yield call(submitOrder, action.data);
        yield put(orderSuccess()) 
    } catch (e) { 
        yield put(orderError()) 
    }
}
function* orderUser(action) {
    try {
        const data = yield call(getOrder, action.email);
        yield put(listOrderUser(data))
    } catch (e) {
        yield put(listOrderUser()) 
    }
}
export function* sendOrder() { 
    yield takeEvery(SUBMIT_ORDER, submit) 
}

export function* getOrderUser() {
    yield takeEvery(GET_ORDER_USER, orderUser) 
}
