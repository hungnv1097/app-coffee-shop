import { all, fork} from 'redux-saga/effects';
import { getProducts } from './product/product';
import { sendOrder, getOrderUser } from './order/order';

export function* rootSaga () {
    yield all([
      getProducts(),
      sendOrder(),
      getOrderUser()
    ]);
};