export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_ERROR = 'ORDER_ERROR';
export const ORDER_USER = 'ORDER_USER';
export const SUBMIT_ORDER = 'SUBMIT_ORDER';
export const GET_ORDER_USER = 'GET_ORDER_USER';

export function orderSuccess() {
    return {
        type: ORDER_SUCCESS
    }
}

export function orderError() {
    return {
        type: ORDER_ERROR
    }
}

export function listOrderUser(data) {
    return {
        type: ORDER_USER,
        data
    }
}

export function submit(data) {
    return {
        type: SUBMIT_ORDER,
        data
    }
}

export function getOrder(email) {
    return {
        type: GET_ORDER_USER,
        email
    }
}