export const ADD_PRODUCT = 'ADD_PRODUCT';
export const ADD_CURRENT = 'ADD_CURRENT';
export const FETCH_PRODUCT = 'FETCH_PRODUCT';

export function fetchData() {
    return { 
        type: FETCH_PRODUCT 
    } 
} 

export function addData(products, blogs) {
    return { 
        type: ADD_PRODUCT,
        products,
        blogs
    } 
}

export function add_current(data) {
    return {
        type: ADD_CURRENT,
        data
    }
}