export const GET_TOKEN = 'GET_TOKEN';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';


export const getToken = (token) => ({
    type: GET_TOKEN,
    token,
});

export const login = (user) => ({
    type: LOGIN,
    user,
});

export const logout = () => ({
    type: LOGOUT
});