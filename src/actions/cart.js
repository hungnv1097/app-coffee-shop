
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const GET_TOTAL = 'GET_TOTAL';


export function addToCart(data) {
    return { 
        type: ADD_TO_CART,
        data
    } 
} 

export function removeItem(id, cost) {
    return { 
        type: REMOVE_ITEM,
        id,
        cost
    } 
}

export function getTotal() {
    return {
        type: GET_TOTAL
    }
}
