import * as axios from 'react-native-axios';
import { HOST } from "./host";

export default instance = axios.create({
  baseURL: HOST,
  timeout: 1000,
  headers: {'X-Custom-Header': 'foobar'}
});