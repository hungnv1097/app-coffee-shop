import React from "react";
import { Platform, TouchableOpacity, Image } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/home";
import ProductScreen from "../screens/product";
import CartScreen from "../screens/cart";
import UserScreen from "../screens/user";
import LoginScreen from '../screens/login';

import { Ionicons } from "@expo/vector-icons";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
});

//Login
const LoginStack = createStackNavigator(
  {
    Login: LoginScreen
  },
  config
);

LoginStack.navigationOptions = {
  tabBarLabel: "Login",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === "ios"
          ? `ios-information-circle${focused ? "" : "-outline"}`
          : "md-information-circle"
      }
    />
  )
};

LoginStack.path = "";

// Home
const HomeStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: "Home",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-home" : "md-home"}
    />
  )
};

HomeStack.path = "";

const ProductStack = createStackNavigator(
  {
    Product: ProductScreen
  },
  config
);

ProductStack.navigationOptions = {
  tabBarLabel: "Product",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-link" : "md-link"}
    />
  )
};

ProductStack.path = "";

const CartStack = createStackNavigator(
  {
    Cart: CartScreen
  },
  config
);

CartStack.navigationOptions = {
  tabBarLabel: "Cart",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-cart" : "md-cart"}
    />
  )
};

CartStack.path = "";

const UserStack = createStackNavigator(
  {
    User: UserScreen
  },
  config
);

UserStack.navigationOptions = {
  tabBarLabel: "User",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-people" : "md-people"}
    />
  )
};

UserStack.path = "";

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  CartStack,
  UserStack
});

tabNavigator.path = "";

const RootStack = createStackNavigator(
  {
    Login: {
      screen: LoginStack,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    Home: {
      screen: tabNavigator,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    Product: {
      screen: ProductStack,
      navigationOptions: ({ navigation }) => ({
        headerRight: () => (
          <TouchableOpacity
            onPress={() => navigation.navigate("Cart")}
            style={{ paddingRight: 15 }}
          >
            <Ionicons name="md-cart" size={32} color="green" />
          </TouchableOpacity>
        )
      })
    },
    // Cart: {
    //   screen: CartStack,
    //   navigationOptions: {
    //     title: "Your Cart",
    //     headerStyle: {
    //       elevation: 0,
    //       shadowOpacity: 0,
    //       borderBottomWidth: 0,
    //     },
    //   }
    // }
  },
  {
    initialRouteName: 'Login'
  }
);
export default RootStack;
